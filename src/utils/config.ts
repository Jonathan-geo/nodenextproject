import convict from 'convict';
import dotenv from 'dotenv';

dotenv.config();

export const config = convict({
  db: {
    database: {
      doc: 'Database name',
      default: 'proj_consult',
      env: 'DB_DATABASE'
    },
    host: {
      doc: 'Database host',
      default: 'jcdso.database.windows.net',
      env: 'DB_HOST'
    },
    username: {
      doc: 'Database authenticating username',
      default: 'jcdso',
      sensitive: true,
      env: 'DB_USERNAME'
    },
    password: {
      doc: 'Database authenticating password',
      default: '1246583Dv7',
      sensitive: true,
      env: 'DB_PASSWORD'
    }
  }
});
