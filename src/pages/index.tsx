import Head from 'next/head';
import React, { useEffect, useState } from 'react';

//Função que obtem o json emitido pela api
const useFetch = url => {
  const [value, setValue] = useState(null);
  const [loading, setLoading] = useState(true);
  const fetchHandler = async () => {
    const res = await fetch(url).then(res => res.json());
    setValue(res);
    setLoading(false);
  };
  useEffect(() => {
    fetchHandler();
  }, []);

  return { value, loading };
};

//**************************************[[Componente - GruposDropdown]]***********************************

const GruposDropdown = () => {
  //Chamada da api grupos(conexão com o db)
  const { value: grupos = [] } = useFetch('/api/grupos');

  //Função setState React
  const [selectedGrupo, setSelectedGrupo] = useState('');

  //Distinct do grupos 
  const distinctGrupos: string[] = Array.from(new Set(grupos?.map(grupo => grupo.grupo)));
 

  //----------Select/Option - [[grupo]]--------------------
  const options = distinctGrupos.map(grupo => {
    return (
      <option key={grupo} value={grupo}>
        {grupo}
      </option>
    );
  });


  //--------Select/Option - [[CNPJ]] -----------------------
  const cnpjOptions = grupos
    ?.filter(grupo => grupo.grupo === selectedGrupo)
    .map(grupo => {
      return (
        <option key={grupo.os} value={grupo.os}>
          {grupo.cnpj}
        </option>
      );
    });

  //--------Select/Option - [[OS]]---------------------------
  const osOptions = grupos
    ?.filter(grupo => grupo.grupo === selectedGrupo)
    .map(grupo => {
      return (
        <option key={grupo.os} value={grupo.os}>
          {grupo.os}
        </option>
      );
    });


  //---------Select/Option - [[CLIENTE]]----------------------
  const clienteOptions = grupos
  ?.filter(grupo => grupo.grupo === selectedGrupo)
  .map(grupo => {
    return (
      <option key={grupo.os} value={grupo.os}>
        {grupo.cliente}
      </option>
    );
  });


  return (
    <>

      {/* --------RENDER-GRUPOS-------- */}
      <p>Grupos</p>
      <select value={selectedGrupo} onChange={event => {setSelectedGrupo(event.target.value);}} name="grupos">
        <option value="" disabled>
          - please select -
        </option>
        {options}
      </select>

      {/* --------RENDER-CNPJ---------- */}

      <>
        <p>CNPJ</p>

        <select name="CNPJ" id="CNPJ">
          {cnpjOptions}
        </select>
      </>


      {/* ---------RENDER-OS------------ */}

      <>
        <p>OS</p>

        <select name="OS" id="OS">
          {osOptions}
        </select>
      </>




      {/* ------RENDER-CLIENTE---------- */}

      <>
        <p>OS</p>

        <select name="OS" id="OS">
          {clienteOptions}
        </select>
      </>


    </>

  ); //end return
}; //end GruposDropdown


//**************************************[[Componente - ProdutosDropdown]]***********************************

const ProdutosDropdown = () => {
  //Chamada da api grupos(conexão com o db)
  const { value: produtos = [] } = useFetch('/api/produtos');

  //----------Select/Option - [[grupo]]--------------------
  const produtosSelect = produtos?.map(produto => {
    return (
      <option key={produto.idProduto} value={produto.idProduto}>
        {produto.produto}
      </option>
    );
  });

  return (
    <>
      {/* --------RENDER-PRODUTOS-------- */}
      <p>PRODUTOS</p>
      <select name="produto" id="produto">
        <option value="" disabled>
          - please select -
        </option>
        {produtosSelect}
      </select>
    </>
  );
}; //end ProdutosDropdown



//***********************************[[Página HOME:  http://localhost:3000/]]**********************************


const Home: React.FC = () => {
  // const { value: produtos = [] } = useFetch('/api/produtos');
  // const { value: cadastros = [] } = useFetch('/api/cadastros');

  return (
    <div className="container">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>

        {/* Titulo e descrissão */}
        <h1 className="title">Formulário TESTE</h1>
        <p>Trabalho avaliativo</p>


        {/* component GruposDropdown - renderiza os dados no "HTML" */}
        <GruposDropdown />

        {/* component ProdutosDropdown - renderiza os dados no "HTML" */}
        <ProdutosDropdown />


      </main>
    </div>
  );
};

export default Home;
